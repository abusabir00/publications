<!DOCTYPE html>
<html lang="en">
    
    <!-- ===Head includes==== --> 
    @include('home.include.head')

    <body class="cnt-home">

        <!-- ===Header includes==== --> 
        @include('home.include.header')
        

        <div class="body-content outer-top-xs" id="top-banner-and-menu">
            <div class="container">

                <div class="row">

                    @yield('sidebar')


                    @yield('content');
                    

                    @yield('sidebar2')
                    

                </div>
                <!-- /.row -->

                <!-- ===Sidebar includes==== --> 
                @include('home.include.brandCarousel')
                


            </div>
            <!-- /.container -->
        </div>
        <!-- /#top-banner-and-menu -->

        <!-- ===Sidebar includes==== --> 
        @include('home.include.footer')

        <!-- ===Sidebar includes==== --> 
        @include('home.include.foot')

    </body>
</html>