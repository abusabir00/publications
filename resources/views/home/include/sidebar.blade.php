<!-- ============================================== SIDEBAR ============================================== -->	

<div class="col-xs-12 col-sm-12 col-md-3 sidebar">

    <!-- ============================================== Testimonials============================================== -->

    <div class="sidebar-widget outer-bottom-small wow fadeInUp ">

        <div id="advertisement" class="advertisement">

            <div class="item">

                <div class="avatar">
                    <img src="{{asset('public/home/images/testimonials/member1.png')}}" alt="Image">
                </div>

                <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>

                <div class="clients_author">John Doe    <span>Abc Company</span>    </div>

                <!-- /.container-fluid -->

            </div>

            <!-- /.item -->

            <div class="item">

                <div class="avatar"><img src="{{asset('public/home/images/testimonials/member3.png')}}" alt="Image"></div>

                <div class="testimonials"><em>"</em>Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>

                <div class="clients_author">Stephen Doe <span>Xperia Designs</span> </div>

            </div>

            <!-- /.item -->

            <div class="item">

                <div class="avatar"><img src="{{asset('public/home/images/testimonials/member2.png')}}" alt="Image"></div>

                <div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>

                <div class="clients_author">Saraha Smith    <span>Datsun &amp; Co</span>    </div>

                <!-- /.container-fluid -->

            </div>

            <!-- /.item -->

        </div>

        <!-- /.owl-carousel -->

    </div>

    <!-- ============================================== Testimonials: END ============================================== -->



    <!-- ==============================================School notice -->

    <div class="sidebar-widget outer-bottom-small wow fadeInUp ">

        <h4 class="sidebar-heading deep-red-text">Notice</h4>

        <ul class="newsticker" id="notice">

            <li>

                <a href="#">

                    ২০১৬-১৭ শিক্ষাবর্ষে প্রিলিমিনারী টু মাস্টার্স (নিয়মিত/প্রাইভেট) প্রোগামে প্রাথমিক আবেদন সম্পর্কিত | প্রকাশকালঃ ১০ জুন, ২০১৮

                </a>

                <p>

                    <span>National University</span>

                    <span class="pull-right">12/12/2018</span>

                </p>

            </li>

            <li>

                <a href="#">

                    ২০১৭ সালের অনার্স ২য় বর্ষ পরীক্ষার ফলাফল প্রকাশ সংক্রান্ত বিজ্ঞপ্তি | প্রকাশকালঃ ৩১ মে, ২০১৮

                </a>

                <p>

                    <span>National University</span>

                    <span class="pull-right">12/12/2018</span>

                </p>

            </li>

            <li>

                <a href="#">

                    তথ্যবিজ্ঞান ও গ্রন্থাগার ব্যবস্থাপনা বিভাগের এম. এ. (ইভনিং) প্রোগ্রামের ২০১৮ সালের ৩১তম ব্যাচের ভর্তি বিজ্ঞপ্তি

                </a>

                <p>

                    <span>University of Dhaka</span>

                    <span class="pull-right">12/12/2018</span>

                </p>

            </li>

            <li>

                <a href="#">

                    জাপানের Nagao Natural Environment Foundation (NEF) বৃত্তির জন্য দরখাস্ত আহবান

                </a>

                <p>

                    <span>University of Dhaka</span>

                    <span class="pull-right">12/12/2018</span>

                </p>

            </li>

            <li>

                <a href="#">

                    ২০১৭ সালের অনার্স ২য় বর্ষ পরীক্ষার ফলাফল পুনঃনিরীক্ষণের আবেদন প্রসঙ্গে | প্রকাশকালঃ ০৩ জুন, ২০১৮

                </a>

                <p>

                    <span>National University</span>

                    <span class="pull-right">12/12/2018</span>

                </p>

            </li>

            <li>

                <a href="#">

                    2nd International Conference- 2018 on "Rohingya: Politics, Ethnic Cleansing and Uncertainty"

                </a>

                <p>

                    <span>University of Dhaka</span>

                    <span class="pull-right">12/12/2018</span>

                </p>

            </li>

        </ul>

    </div>

    <!-- ============================================== School notice : END ============================================== -->



    <!-- ============================================== BOARD LINK============================================== -->

    <div class="sidebar-widget outer-bottom-small wow fadeInUp ">

        <h4 class="sidebar-heading deep-green-text">Institute Links</h4>

        <ul class="links-list" id="">

            <li>

                <a href="http://www.du.ac.bd/">

                    University of Dhaka

                </a>

            </li>

            <li>

                <a href="http://www.buet.ac.bd/">

                    BUET: Bangladesh University of Engineering and Technology

                </a>

            </li>

            <li>

                <a href="http://www.cu.ac.bd/ctguni/">

                    University of Chittagong

                </a>

            </li>

            <li>

                <a href="http://www.barisaluniv.edu.bd/">

                    University of Barisal

                </a>

            </li>

            <li>

                <a href="http://www.juniv.edu/">

                    Jahangirnagar University

                </a>

            </li>

        </ul>

    </div>

    <!-- ============================================== BOARD LINK: END ============================================== -->



</div>

<!-- /.sidemenu-holder -->