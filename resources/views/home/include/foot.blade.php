<!-- For demo purposes – can be removed on production -->
<!-- For demo purposes – can be removed on production : End -->
<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="{{asset('/public/home/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('/public/home/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/public/home/js/bootstrap-hover-dropdown.min.js')}}"></script>
<script src="{{asset('/public/home/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('/public/home/js/echo.min.js')}}"></script>
<script src="{{asset('/public/home/js/jquery.easing-1.3.min.js')}}"></script>
<script src="{{asset('/public/home/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('/public/home/js/jquery.rateit.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/public/home/js/lightbox.min.js')}}"></script>
<script src="{{asset('/public/home/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('/public/home/js/wow.min.js')}}"></script>
<script src="{{asset('/public/home/js/jquery.newsTicker.js')}}"></script>
<script src="{{asset('/public/home/js/scripts.js')}}"></script>

@yield('js')