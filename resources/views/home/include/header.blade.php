<!-- ============================================== HEADER ============================================== -->

<header class="header-style-1">

    <div class="main-header">

        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">

                    <!-- ============================================================= LOGO ============================================================= -->

                    <div class="logo">

                        <a href="{{url('/')}}">

                        <img class="top-logo" src="{{asset('/public/home/images/bat.png')}}" alt="Fulkuri Publications">

                        </a>

                    </div>

                    <!-- /.logo -->

                    <!-- ============================================================= LOGO : END ============================================================= -->				

                </div>

                <!-- /.logo-holder -->

                <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">

                    <!-- /.contact-row -->

                    <!-- ============================================================= SEARCH AREA ============================================================= -->

                    <div class="search-area">

                        <form>

                            <div class="control-group">

                                <ul class="categories-filter animate-dropdown">

                                    <li class="dropdown">

                                        <a class="dropdown-toggle"  data-toggle="dropdown" href="category.html">Categories <b class="caret"></b></a>

                                        <ul class="dropdown-menu" role="menu" >

                                            <!-- <li class="menu-header">Computer</li> -->

                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('/products_list')}}">- Clothing</a></li>

                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('/products_list')}}">- Electronics</a></li>

                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('/products_list')}}">- Shoes</a></li>

                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('/products_list')}}">- Watches</a></li>

                                        </ul>

                                    </li>

                                </ul>

                                <input class="search-field" placeholder="Search here..." />

                                <a class="search-button" href="{{url('/products_list')}}" ></a>    

                            </div>

                        </form>

                    </div>

                    <!-- /.search-area -->

                    <!-- ============================================================= SEARCH AREA : END ============================================================= -->				

                </div>

                <!-- /.top-search-holder -->

                <div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">

                    <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->

                    <div class="dropdown dropdown-cart">

                        <a href="{{url('/products_list')}}" class="dropdown-toggle lnk-cart" data-toggle="dropdown">

                            <div class="items-cart-inner">

                                <div class="basket">

                                    <i class="glyphicon glyphicon-shopping-cart"></i>

                                </div>

                                <div class="basket-item-count"><span class="count">2</span></div>

                                <div class="total-price-basket">

                                    <span class="lbl">cart -</span>

                                    <span class="total-price">

                                    <span class="sign">$</span><span class="value">600.00</span>

                                    </span>

                                </div>

                            </div>

                        </a>

                        <ul class="dropdown-menu">

                            <li>

                                <div class="cart-item product-summary">

                                    <div class="row">

                                        <div class="col-xs-4">

                                            <div class="image">

                                                <a href="detail.html"><img src="{{asset('public/home/images/cart.jpg')}}" alt=""></a>

                                            </div>

                                        </div>

                                        <div class="col-xs-7">

                                            <h3 class="name"><a href="index.php?page-detail">Simple Product</a></h3>

                                            <div class="price">$600.00</div>

                                        </div>

                                        <div class="col-xs-1 action">

                                            <a href="{{url('/products_list')}}"><i class="fa fa-trash"></i></a>

                                        </div>

                                    </div>

                                </div>

                                <!-- /.cart-item -->

                                <div class="clearfix"></div>

                                <hr>

                                <div class="clearfix cart-total">

                                    <div class="pull-right">

                                        <span class="text">Sub Total :</span><span class='price'>$600.00</span>

                                    </div>

                                    <div class="clearfix"></div>

                                    <a href="checkout.html" class="btn btn-upper btn-primary btn-block m-t-20">Checkout</a>	

                                </div>

                                <!-- /.cart-total-->

                            </li>

                        </ul>

                        <!-- /.dropdown-menu-->

                    </div>

                    <!-- /.dropdown-cart -->

                    <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->				

                </div>

                <!-- /.top-cart-row -->

            </div>

            <!-- /.row -->

        </div>

        <!-- /.container -->

    </div>

    <!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->

    <div class="header-nav animate-dropdown">

        <div class="container">

            <div class="yamm navbar navbar-default" role="navigation">

                <div class="navbar-header">

                    <button data-target="mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    </button>

                </div>

                <div class="nav-bg-class">

                    <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">

                        <div class="nav-outer">

                            <ul class="nav navbar-nav">

                                <!-- <li class="active dropdown yamm-fw">

                                    <a href="home.html" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">Home</a>

                                </li> -->

                                <li class="dropdown yamm mega-menu">

                                    <a href="home.html" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">লেখক </a>

                                    <ul class="dropdown-menu container">

                                        <li>

                                            <div class="yamm-content ">

                                                <div class="row">

                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-menu">

                                                        <!-- <h2 class="title">Men</h2> -->

                                                        <ul class="links">

                                                            <li><a href="{{url('/products_list')}}">হুমায়ূন আহমেদ </a></li>

                                                            <li><a href="{{url('/products_list')}}"> মুহম্মদ জাফর ইকবাল </a></li>

                                                            <li><a href="{{url('/products_list')}}"> সমরেশ মজুমদার</a></li>

                                                            <li><a href="{{url('/products_list')}}"> রবীন্দ্রনাথ ঠাকুর</a></li>

                                                            <li><a href="{{url('/products_list')}}"> সুনীল গঙ্গোপাধ্যায়</a></li>

                                                            <li><a href="{{url('/products_list')}}"> আনিসুল হক</a></li>

                                                            <li><a href="{{url('/products_list')}}"> শীর্ষেন্দু মুখোপাধ্যায়</a></li>

                                                        </ul>

                                                    </div>

                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-menu">

                                                        <ul class="links">

                                                            <li><a href="{{url('/products_list')}}">আহমদ ছফা</a></li>

                                                            <li><a href="{{url('/products_list')}}"> বিভূতিভূষণ বন্দ্যোপাধ্যায়</a></li>

                                                            <li><a href="{{url('/products_list')}}"> সৈয়দ শামসুল হক </a></li>

                                                            <li><a href="{{url('/products_list')}}">জহির রায়হান</a></li>

                                                            <li><a href="{{url('/products_list')}}">তামিম শাহরিয়ার সুবিন</a></li>

                                                            <li><a href="{{url('/products_list')}}">কাজী নজরুল ইসলাম</a></li>

                                                            <li><a href="{{url('/products_list')}}">হুমায়ুন আজাদ</a></li>

                                                        </ul>

                                                    </div>

                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-menu">

                                                        <ul class="links">

                                                            <li><a href="{{url('/products_list')}}">ড্যান ব্রাউন</a></li>

                                                            <li><a href="{{url('/products_list')}}">চেতন ভগত</a></li>

                                                            <li><a href="{{url('/products_list')}}">রকিব হাসান</a></li>

                                                            <li><a href="{{url('/products_list')}}">সৈয়দ মুজতবা আলী</a></li>

                                                            <li><a href="{{url('/products_list')}}">মানিক বন্দ্যোপাধ্যায়</a></li>

                                                            <li><a href="{{url('/products_list')}}">বঙ্কিমচন্দ্র চট্টোপাধ্যায়</a></li>

                                                            <li><a href="{{url('/products_list')}}">আখতারুজ্জামান ইলিয়াস</a></li>

                                                        </ul>

                                                    </div>

                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-menu">

                                                        <ul class="links">

                                                            <li><a href="{{url('/products_list')}}">সেলিনা হোসেন</a></li>

                                                            <li><a href="{{url('/products_list')}}">স্টিফেন কিং</a></li>

                                                            <li><a href="{{url('/products_list')}}">জে. কে. রাওলিং</a></li>

                                                            <li><a href="{{url('/products_list')}}">কাজী আনোয়ার হোসেন</a></li>

                                                            <li><a href="{{url('/products_list')}}"> সুমন্ত আসলাম</a></li>

                                                            <li><a href="{{url('/products_list')}}">পাওলো কোয়েলহো</a></li>

                                                            <li><a href="{{url('/products_list')}}">শওকত ওসমান</a></li>

                                                        </ul>



                                                        <a class="menu_read_more" href="{{url('/products_list')}}">See More...</a>

                                                    </div>

                                                    <!-- /.col -->

                                                    <!-- /.yamm-content -->					

                                                </div>

                                            </div>

                                        </li>

                                    </ul>

                                </li>

                                <li class="dropdown mega-menu">

                                    <a href="category.html"  data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">বিষয় 

                                    </a>

                                    <ul class="dropdown-menu container">

                                        <li>

                                            <div class="yamm-content">

                                                <div class="row">

                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-menu">

                                                        <!-- <h2 class="title">Men</h2> -->

                                                        <ul class="links">

                                                            <li><a href="{{url('/products_list')}}">হুমায়ূন আহমেদ </a></li>

                                                            <li><a href="{{url('/products_list')}}"> মুহম্মদ জাফর ইকবাল </a></li>

                                                            <li><a href="{{url('/products_list')}}"> সমরেশ মজুমদার</a></li>

                                                            <li><a href="{{url('/products_list')}}"> রবীন্দ্রনাথ ঠাকুর</a></li>

                                                            <li><a href="{{url('/products_list')}}"> সুনীল গঙ্গোপাধ্যায়</a></li>

                                                            <li><a href="{{url('/products_list')}}"> আনিসুল হক</a></li>

                                                            <li><a href="{{url('/products_list')}}"> শীর্ষেন্দু মুখোপাধ্যায়</a></li>

                                                        </ul>

                                                    </div>

                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-menu">

                                                        <ul class="links">

                                                            <li><a href="{{url('/products_list')}}">আহমদ ছফা</a></li>

                                                            <li><a href="{{url('/products_list')}}"> বিভূতিভূষণ বন্দ্যোপাধ্যায়</a></li>

                                                            <li><a href="{{url('/products_list')}}"> সৈয়দ শামসুল হক </a></li>

                                                            <li><a href="{{url('/products_list')}}">জহির রায়হান</a></li>

                                                            <li><a href="{{url('/products_list')}}">তামিম শাহরিয়ার সুবিন</a></li>

                                                            <li><a href="{{url('/products_list')}}">কাজী নজরুল ইসলাম</a></li>

                                                            <li><a href="{{url('/products_list')}}">হুমায়ুন আজাদ</a></li>

                                                        </ul>

                                                    </div>

                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-menu">

                                                        <ul class="links">

                                                            <li><a href="{{url('/products_list')}}">ড্যান ব্রাউন</a></li>

                                                            <li><a href="{{url('/products_list')}}">চেতন ভগত</a></li>

                                                            <li><a href="{{url('/products_list')}}">রকিব হাসান</a></li>

                                                            <li><a href="{{url('/products_list')}}">সৈয়দ মুজতবা আলী</a></li>

                                                            <li><a href="{{url('/products_list')}}">মানিক বন্দ্যোপাধ্যায়</a></li>

                                                            <li><a href="{{url('/products_list')}}">বঙ্কিমচন্দ্র চট্টোপাধ্যায়</a></li>

                                                            <li><a href="{{url('/products_list')}}">আখতারুজ্জামান ইলিয়াস</a></li>

                                                        </ul>

                                                    </div>

                                                    <!-- /.col -->

                                                    <div class="col-xs-12 col-sm-6 col-md-3 col-menu">

                                                        <ul class="links">

                                                            <li><a href="{{url('/products_list')}}">সেলিনা হোসেন</a></li>

                                                            <li><a href="{{url('/products_list')}}">স্টিফেন কিং</a></li>

                                                            <li><a href="{{url('/products_list')}}">জে. কে. রাওলিং</a></li>

                                                            <li><a href="{{url('/products_list')}}">কাজী আনোয়ার হোসেন</a></li>

                                                            <li><a href="{{url('/products_list')}}"> সুমন্ত আসলাম</a></li>

                                                            <li><a href="{{url('/products_list')}}">পাওলো কোয়েলহো</a></li>

                                                            <li><a href="{{url('/products_list')}}">শওকত ওসমান</a></li>

                                                        </ul>



                                                        <a class="menu_read_more" href="{{url('/products_list')}}">See More...</a>

                                                    </div>

                                                </div>

                                                <!-- /.row -->

                                            </div>

                                            <!-- /.yamm-content -->					

                                        </li>

                                    </ul>

                                </li>

                                <li class="dropdown hidden-sm">

                                    <a href="category.html">বেস্টসেলার বই 

                                    </a>

                                </li>

                                <li class="dropdown hidden-sm">

                                    <a href="category.html">ইসলামি বই</a>

                                </li>

                                <li class="dropdown">

                                    <a href="contact.html">অতিরিক্ত ছাড়ের বই</a>

                                </li>

                                <li class="dropdown">

                                    <a href="contact.html">বইমেলা ২০১৮</a>

                                </li>

                                <li class="dropdown">

                                    <a href="contact.html">পশ্চিমবঙ্গের বই</a>

                                </li>

                            </ul>

                            <!-- /.navbar-nav -->

                            <div class="clearfix"></div>

                        </div>

                        <!-- /.nav-outer -->

                    </div>

                    <!-- /.navbar-collapse -->

                </div>

                <!-- /.nav-bg-class -->

            </div>

            <!-- /.navbar-default -->

        </div>

        <!-- /.container-class -->

    </div>

    <!-- /.header-nav -->

    <!-- ============================================== NAVBAR : END ============================================== -->

</header>

<!-- ============================================== HEADER : END ============================================== -->