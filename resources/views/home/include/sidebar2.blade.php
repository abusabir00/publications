<!-- ============================================== SIDEBAR ============================================== -->  
<div class="col-xs-12 col-sm-12 col-md-3 sidebar">
    <!-- ============================================== LATEST NEWS============================================== -->
    <div class="sidebar-widget outer-bottom-small wow fadeInUp ">
        <h4 class="sidebar-heading deep-blue-text">latest news</h4>
        <ul class="newsticker" id="latest-news">
            <li>
                <a href="#">
                    ঢাবি প্রো-ভাইস চ্যান্সেলর পদে যোগদান করেছেন অধ্যাপক ড. মুহাম্মদ সামাদ
                </a>
                <p>
                    <span>Source Name </span>
                    <span class="pull-right">12/12/2018</span>
                </p>
            </li>
            <li>
                <a href="#">
                    Curabitur porttitor ante eget hendrerit adipiscing.
                </a>
                <p>
                    <span>Source Name </span>
                    <span class="pull-right">12/12/2018</span>
                </p>
            </li>
            <li>
                <a href="#">
                    Praesent ornare nisl lorem, ut condimentum lectus gravida ut.
                </a>
                <p>
                    <span>Source Name </span>
                    <span class="pull-right">12/12/2018</span>
                </p>
            </li>
            <li>
                <a href="#">
                    Nunc ultrices tortor eu massa placerat posuere.
                </a>
                <p>
                    <span>Source Name </span>
                    <span class="pull-right">12/12/2018</span>
                </p>
            </li>
        </ul>
    </div>
    <!-- ============================================== LATEST NEWS: END ============================================== -->

    <!-- ============================================== VIDEOS ============================================== -->
    <div class="sidebar-widget outer-bottom-small wow fadeInUp">
        <h3 class="sidebar-heading deep-green-text no-margin">Videos</h3>
        <div class="sidebar-widget-body outer-top-xs m-t-10">
            <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs m-t-0">
                <div class="item">
                    <div class="video-item">
                        <iframe src="https://www.youtube.com/embed/yORLdlmq0pU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="item">
                    <div class="video-item">
                        <iframe src="https://www.youtube.com/embed/N5PnlTnpPLo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="item">
                    <div class="video-item">
                        <iframe src="https://www.youtube.com/embed/pAtKAG8bZQk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.sidebar-widget-body -->
    </div>
    <!-- /.sidebar-widget -->
    <!-- ============================================== VIDEOS : END ============================================== -->

    <!-- ============================================== BOARD LINK============================================== -->
    <div class="sidebar-widget outer-bottom-small wow fadeInUp ">
        <h4 class="sidebar-heading deep-blue-text">Board Links</h4>
        <ul class="links-list" id="">
            <li>
                <a href="http://dhakaeducationboard.gov.bd/">
                    Board of Intermediate and Secondary Education, Dhaka
                </a>
            </li>
            <li>
                <a href="http://www.jessoreboard.gov.bd/">
                    Board of Intermediate and Secondary Education, Jessore
                </a>
            </li>
            <li>
                <a href="http://dinajpureducationboard.gov.bd/">
                    মাধ্যমিক ও উচ্চ মাধ্যমিক শিক্ষা বোর্ড, দিনাজপুর
                </a>
            </li>
            <li>
                <a href="http://www.barisalboard.gov.bd/">
                    Board of Intermediate and Secondary Education, Barishal
                </a>
            </li>
            <li>
                <a href="http://dhakaeducationboard.gov.bd/">
                    Board of Intermediate and Secondary Education, Dhaka
                </a>
            </li>
        </ul>
    </div>
    <!-- ============================================== BOARD LINK: END ============================================== -->

</div>
<!-- /.sidemenu-holder -->