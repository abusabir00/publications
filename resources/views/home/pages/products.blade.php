@extends('home.homeMaster')

@section('title')
Books List Page
@endsection

@section('css')

@endsection

@section('content')

<!-- /.breadcrumb -->
<div class="body-content outer-top-xs list_page">
<div class="container">
    <div class='row '>
        <div class='col-md-12'>
            <section class="section featured-product wow fadeInUp authorHeader">
                <div class="detail-block">
                    <img class="authImg" src="https://s3-ap-southeast-1.amazonaws.com/rokomari110/people/2ca0d63a24f4_4279.jpg" alt="Humayun Ahmed books">
                    
                    <div class="authDes">
                        <div><h1>স্টিফেন কিং</h1></div>
                        <p class="des">Stephen Edwin King (born September 21, 1947) is an American author of horror, supernatural fiction, suspense, science fiction, and fantasy. His books have sold more than 350 million copies, many of which have been adapted into feature films, miniseries, television shows, and comic books. King has published 54 novels, including seven under the pen name Richard Bachman, and six non-fiction books. He...</p>
                        <a href="#" class="desSeeMore" style="display: inline;">See More</a>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class='container'>
    <div class='row single-product'>
        <div class='col-md-2 p-r-0'>
            <div class="sidebar-widget sort_sidebar">
                <div class="outer-bottom-small wow fadeInUp">
                    <a class="" role="button" data-toggle="collapse" href="#Category" aria-expanded="true" aria-controls="Category">
                        <span class="sort_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        Shop By Category(58)
                    </a>
                    <div class="collapse in m-t-15" id="Category">
                        <form action="">
                            <div class="input-group">
                                <input type="text" class="form-control" aria-describedby="basic-addon2">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </span>
                            </div>

                            <div class="sorting_list">
                                <ul>
                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="outer-bottom-small wow fadeInUp">
                    <a class="" role="button" data-toggle="collapse" href="#Publisher" aria-expanded="true" aria-controls="Publisher">
                        <span class="sort_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        Shop By Publisher(12)
                    </a>
                    <div class="collapse in m-t-15" id="Publisher">
                        <form action="">
                            <div class="input-group">
                                <input type="text" class="form-control" aria-describedby="basic-addon2">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                </span>
                            </div>

                            <div class="sorting_list">
                                <ul>
                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">সমকালীন উপন্যাস
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="outer-bottom-small wow fadeInUp">
                    <a class="" role="button" data-toggle="collapse" href="#Price" aria-expanded="true" aria-controls="Price">
                        <span class="sort_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        Shop By Price
                    </a>
                    <div class="collapse in" id="Price">
                        <form action="">
                            <div class="sorting_list">
                                <ul>
                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">Tk. 10 - 8420
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">Tk. 8421 - 16840
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">Tk. 16841 - 25260
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">Tk. 25261 - 33670
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">Tk. 33671 - 42080
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="outer-bottom-small wow fadeInUp">
                    <a class="" role="button" data-toggle="collapse" href="#Language" aria-expanded="true" aria-controls="Language">
                        <span class="sort_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        Shop By Language
                    </a>
                    <div class="collapse in" id="Language">
                        <form action="">
                            <div class="sorting_list">
                                <ul>
                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">বাংলা
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">ইংরেজি
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">আরবী
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="outer-bottom-small wow fadeInUp">
                    <a class="" role="button" data-toggle="collapse" href="#Country" aria-expanded="true" aria-controls="Country">
                        <span class="sort_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        Shop By Country
                    </a>
                    <div class="collapse in" id="Country">
                        <form action="">
                            <div class="sorting_list">
                                <ul>
                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">বাংলাদেশ
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">ভারত
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">বিদেশী
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="outer-bottom-small wow fadeInUp">
                    <a class="" role="button" data-toggle="collapse" href="#Rating" aria-expanded="true" aria-controls="Rating">
                        <span class="sort_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        Shop By Rating
                    </a>
                    <div class="collapse in" id="Rating">
                        <form action="">
                            <div class="sorting_list">
                                <ul>
                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox"><span class="glyphicon glyphicon-star"></span>
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                              <span class="glyphicon glyphicon-star"></span>
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class='col-md-10'>
            <!-- /Product Info -->
            <ul class="list-inline list-unstyled viewRow">
                <li>Showing 1 to 60 of 486</li>

                <li class="pull-right">
                    <form class="navbar-form p-r-0" role="search">
                        <div class="form-group base">
                            <input type="text" id="secondSearchBox" class="form-control searchInput ui-autocomplete-input" placeholder="Enter your keyword" onkeydown="Javascript: if (event.keyCode==13) clickAction();" autocomplete="off">
                            <button type="submit" class="btn btnSearchSubmit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </li>
            </ul>

            <!-- Product Tabs -->
            <ul class="list-inline list-unstyled menuRow">
                <li class="" id="orderAll" onclick="changeOrder('0')">All</li>
                <li id="orderNew" onclick="changeOrder('1')" class="activeMenu">New Released</li>
                <li id="orderBestSeller" onclick="changeOrder('2')">Best Sellers</li>
            </ul>
            <!-- ============================================== UPSELL PRODUCTS ============================================== -->
            <section class="section featured-product wow fadeInUp">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="products p-lr-15">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home')}}/images/products/book-01.jpg" alt=""></a>
                                    </div>
                                    <!-- /.image -->            
                                    
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price"> 
                                        <span class="price">
                                        TK.650.99               </span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>                                                 
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                </div>
            </section>
            <!-- /.section -->

            <nav aria-label="Page navigation">
                <ul class="pagination pull-right">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
        </div>
        <!-- /.col -->
        <div class="clearfix"></div>
    </div>
    <!-- /.row -->
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
        <div class="logo-slider-inner">
            <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                <div class="item m-t-15">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand1.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item m-t-10">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand2.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand3.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand4.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand5.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand6.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand2.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand4.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand1.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand5.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
            </div>
            <!-- /.owl-carousel #logo-slider -->
        </div>
        <!-- /.logo-slider-inner -->
    </div>
    <!-- /.logo-slider -->
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	
</div>
<!-- /.container -->

</div>
<!-- /.body-content -->

@endsection

@section('js')

@endsection