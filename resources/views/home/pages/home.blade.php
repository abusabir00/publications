@extends('home.homeMaster')

@section('title')
Books Page
@endsection

@section('css')

@endsection

@section('sidebar')
<!-- ===Sidebar includes==== --> 
@include('home.include.sidebar') 
@endsection



@section('content')

<!-- ============================================== CONTENT ============================================== -->
<div class="col-xs-12 col-sm-12 col-md-7 homebanner-holder">
    <!-- ========================================== SECTION – HERO ========================================= -->
    <div id="hero">
        <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
            <div class="item" style="background-image: url({{asset('/public/home/images/sliders/slider-01.jpg')}});">
                <div class="container-fluid">
                    <div class="caption bg-color vertical-center text-left">
                        <div class="slider-header fadeInDown-1">Top Brands</div>
                        <div class="big-text fadeInDown-1">
                            New Collections
                        </div>
                        <div class="excerpt fadeInDown-2 hidden-xs">
                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </div>
                        <div class="button-holder fadeInDown-3">
                            <a href="index.php?page=single-product" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a>
                        </div>
                    </div>
                    <!-- /.caption -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.item -->
            <div class="item" style="background-image: url({{asset('/public/home/images/sliders/slider-02.jpg')}});">
                <div class="container-fluid">
                    <div class="caption bg-color vertical-center text-left">
                        <div class="slider-header fadeInDown-1">Spring 2016</div>
                        <div class="big-text fadeInDown-1">
                            Women <span class="highlight">Fashion</span>
                        </div>
                        <div class="excerpt fadeInDown-2 hidden-xs">
                            <span>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit</span>
                        </div>
                        <div class="button-holder fadeInDown-3">
                            <a href="index.php?page=single-product" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a>
                        </div>
                    </div>
                    <!-- /.caption -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.item -->
        </div>
        <!-- /.owl-carousel -->
    </div>
    <!-- ========================================= SECTION – HERO : END ========================================= -->	
    <!-- ============================================== INFO BOXES ============================================== -->
    <div class="info-boxes wow fadeInUp">
        <div class="info-boxes-inner">
            <div class="row">
                <div class="col-md-6 col-sm-4 col-lg-4">
                    <div class="info-box">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4 class="info-box-heading green">money back</h4>
                            </div>
                        </div>
                        <h6 class="text">30 Days Money Back Guarantee</h6>
                    </div>
                </div>
                <!-- .col -->
                <div class="hidden-md col-sm-4 col-lg-4">
                    <div class="info-box">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4 class="info-box-heading green">free shipping</h4>
                            </div>
                        </div>
                        <h6 class="text">Shipping on orders over $99</h6>
                    </div>
                </div>
                <!-- .col -->
                <div class="col-md-6 col-sm-4 col-lg-4">
                    <div class="info-box">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4 class="info-box-heading green">Special Sale</h4>
                            </div>
                        </div>
                        <h6 class="text">Extra $5 off on all items </h6>
                    </div>
                </div>
                <!-- .col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.info-boxes-inner -->
    </div>
    <!-- /.info-boxes -->
    <!-- ============================================== INFO BOXES : END ============================================== -->


    <!-- ============================================== FEATURED PRODUCTS ============================================== -->
    <section class="section featured-product outer-top-vs wow fadeInUp">
        <h3 class="section-title">Featured products</h3>
        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->			
                            <div class="tag hot"><span>hot</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price">	
                                <span class="price">
                                $450.99				</span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>													
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->			
                            <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price">	
                                <span class="price">
                                $450.99				</span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>													
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/blank.gif')}}" data-echo="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->			
                            <div class="tag sale"><span>sale</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price">	
                                <span class="price">
                                $450.99				</span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>													
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->			
                            <div class="tag hot"><span>hot</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price">	
                                <span class="price">
                                $450.99				</span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>													
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->			
                            <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price">	
                                <span class="price">
                                $450.99				</span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>													
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->			
                            <div class="tag sale"><span>sale</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price">	
                                <span class="price">
                                $450.99				</span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>													
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
        </div>
        <!-- /.home-owl-carousel -->
    </section>
    <!-- /.section -->
    <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

    <!-- ============================================== FEATURED PRODUCTS ============================================== -->
    <section class="section featured-product wow fadeInUp new-arriavls">
        <h3 class="section-title">New Arrivals</h3>
        <div class="owl-carousel Arrivals-owl-carousel custom-carousel owl-theme outer-top-xs">
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->            
                            <div class="tag hot"><span>hot</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price"> 
                                <span class="price">
                                $450.99             </span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>                                                 
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->            
                            <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price"> 
                                <span class="price">
                                $450.99             </span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>                                                 
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/blank.gif')}}" data-echo="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->            
                            <div class="tag sale"><span>sale</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price"> 
                                <span class="price">
                                $450.99             </span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>                                                 
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->            
                            <div class="tag hot"><span>hot</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price"> 
                                <span class="price">
                                $450.99             </span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>                                                 
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->            
                            <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price"> 
                                <span class="price">
                                $450.99             </span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>                                                 
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
            <div class="item item-carousel">
                <div class="products">
                    <div class="product">
                        <div class="product-image">
                            <div class="image">
                                <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                            </div>
                            <!-- /.image -->            
                            <div class="tag sale"><span>sale</span></div>
                        </div>
                        <!-- /.product-image -->
                        <div class="product-info text-left">
                            <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                            <div class="rating rateit-small"></div>
                            <div class="description"></div>
                            <div class="product-price"> 
                                <span class="price">
                                $450.99             </span>
                                <span class="price-before-discount">$ 800</span>
                            </div>
                            <!-- /.product-price -->
                        </div>
                        <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    <li class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                        <i class="fa fa-shopping-cart"></i>                                                 
                                        </button>
                                        <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                    </li>
                                    <li class="lnk wishlist">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                        <i class="icon fa fa-heart"></i>
                                        </a>
                                    </li>
                                    <li class="lnk">
                                        <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                        <i class="fa fa-signal" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        <!-- /.cart -->
                    </div>
                    <!-- /.product -->
                </div>
                <!-- /.products -->
            </div>
            <!-- /.item -->
        </div>
        <!-- /.home-owl-carousel -->
    </section>
    <!-- /.section -->
    <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->
</div>
<!-- /.homebanner-holder -->
<!-- ============================================== CONTENT : END ============================================== -->
@endsection

@section('sidebar2')
<!-- ===Sidebar includes==== --> 
@include('home.include.sidebar2')
@endsection

@section('js')

@endsection