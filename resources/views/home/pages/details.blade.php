@extends('home.homeMaster')

@section('title')
Books Details Page
@endsection

@section('css')

@endsection

@section('content')

<!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
<div class='container'>
    <div class='row single-product'>
        <div class='col-md-12'>
            <div class="detail-block">
                <div class="row  wow fadeInUp">
                    <div class="col-xs-12 col-sm-6 col-md-4 gallery-holder">
                        <div class="product-item-holder size-big single-product-gallery small-gallery">
                            <div id="owl-single-product">
                                <div class="single-product-gallery-item details_book_wrapper">
                                    <a data-toggle="modal" data-target="#details_modal" href="javascript:;">
                                        <img class="img-responsive" alt="" src="{{asset('/public/home/images/blank.gif')}}" data-echo="{{asset('/public/home/images/books/book.jpg')}}" />
                                    </a>
                                </div>
                                <!-- /.single-product-gallery-item -->
                            </div>
                            <!-- /.single-product-slider -->
                        </div>
                        <!-- /.single-product-gallery -->
                    </div>
                    <!-- /.gallery-holder -->        			
                    <div class='col-sm-6 col-md-8 product-info-block'>
                        <div class="product-info">
                            <h1 class="name">The Road Less Travelled: A New Psychology of Love, Traditional Values and Spiritual Growth (Paperback)</h1>
                            <div class="writter">
                                <a href="#">M. Scott Peck</a>
                            </div>
                            <div class="price-container info-container m-t-20">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="price-box">
                                            <span class="price">TK. 800.00</span>
                                            <span class="price-strike">TK. 900.00</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.price-container -->
                            <div class="rating-reviews m-t-20">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="rating rateit-small"></div>
                                    </div>
                                </div>
                                <!-- /.row -->		
                            </div>
                            <!-- /.rating-reviews -->
                            <div class="stock-container info-container m-t-10">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="stock-box">
                                            <span class="label">Availability :</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="stock-box">
                                            <span class="value">In Stock</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->	
                            </div>
                            <!-- /.stock-container -->
                            <div class="stock-container info-container m-t-10">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="stock-box">
                                            <span class="label">Category :</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="stock-box">
                                            <span class="category"><a href="">Non-Fiction</a></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->	
                            </div>
                            <!-- /.stock-container -->
                            <div class="stock-container info-container m-t-10">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="stock-box add-to-booklist">
                                            <span class="label">
                                            <a href="javascript:;">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>&nbsp;
                                            Add to Booklist
                                            </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->	
                            </div>
                            <!-- /.stock-container -->
                            <div class="quantity-container info-container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-3 col-lg-3">
                                        <a data-target="#details_modal" data-toggle="modal" href="javascript:;" class="btn btn-pink btn-block">একটু পড়ে দেখুন</a>
                                    </div>
                                    <div class="col-sm-6 col-md-3 col-lg-3">
                                        <a href="javascript:;" class="btn btn-block btn-highlight"><i class="fa fa-shopping-cart inner-right-vs"></i> ADD TO CART</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.quantity-container -->
                        </div>
                        <!-- /.product-info -->
                    </div>
                    <!-- /.col-sm-7 -->
                </div>
                <!-- /.row -->
            </div>
            <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="section-title">Product Specification & Summary</h3>
                    </div>
                </div>
                <div class="row no-margin">
                    <div class="col-sm-3">
                        <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                            <li class="active"><a data-toggle="tab" href="#description">Specification</a></li>
                            <li><a data-toggle="tab" href="#review">REVIEW</a></li>
                        </ul>
                        <!-- /.nav-tabs #product-tabs -->
                    </div>
                    <div class="col-sm-9">
                        <div class="tab-content">
                            <div id="description" class="tab-pane in active no-padding">
                                <div class="product-tab">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>Title</td>
                                                <td>The Road Less Travelled: A New Psychology of Love, Traditional Values and Spiritual Growth</td>
                                            </tr>
                                            <tr>
                                                <td>Author</td>
                                                <td class="author-link">
                                                    <a class="lnk-1" href="/book/author/48575">
                                                    M. Scott Peck&nbsp; 
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Publisher</td>
                                                <td class="publisher-link">
                                                    <a class="lnk-1" href="/book/publisher/1061">
                                                    Arrow Books
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>ISBN</td>
                                                <td>9780099727408</td>
                                            </tr>
                                            <tr>
                                                <td>Edition</td>
                                                <td>1st edition, 2006</td>
                                            </tr>
                                            <tr>
                                                <td>Number of Pages</td>
                                                <td>308</td>
                                            </tr>
                                            <tr>
                                                <td>Country</td>
                                                <td>India</td>
                                            </tr>
                                            <tr>
                                                <td>Language</td>
                                                <td>English</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div id="review" class="tab-pane no-padding">
                                <div class="product-tab">
                                    <div class="product-reviews">
                                        <h4 class="title">Customer Reviews</h4>
                                        <div class="reviews">
                                            <div class="review">
                                                <div class="review-title"><span class="summary">We love this product</span><span class="date"><i class="fa fa-calendar"></i><span>1 days ago</span></span></div>
                                                <div class="text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit.Aliquam suscipit."</div>
                                            </div>
                                        </div>
                                        <!-- /.reviews -->
                                    </div>
                                    <!-- /.product-reviews -->
                                    <div class="product-add-review">
                                        <h4 class="title">Write your own review</h4>
                                        <div class="review-table">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th class="cell-label">&nbsp;</th>
                                                            <th>1 star</th>
                                                            <th>2 stars</th>
                                                            <th>3 stars</th>
                                                            <th>4 stars</th>
                                                            <th>5 stars</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="cell-label">Quality</td>
                                                            <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="cell-label">Price</td>
                                                            <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="cell-label">Value</td>
                                                            <td><input type="radio" name="quality" class="radio" value="1"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="2"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="3"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="4"></td>
                                                            <td><input type="radio" name="quality" class="radio" value="5"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- /.table .table-bordered -->
                                            </div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.review-table -->
                                        <div class="review-form">
                                            <div class="form-container">
                                                <form role="form" class="cnt-form">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputName">Your Name <span class="astk">*</span></label>
                                                                <input type="text" class="form-control txt" id="exampleInputName" placeholder="">
                                                            </div>
                                                            <!-- /.form-group -->
                                                            <div class="form-group">
                                                                <label for="exampleInputSummary">Summary <span class="astk">*</span></label>
                                                                <input type="text" class="form-control txt" id="exampleInputSummary" placeholder="">
                                                            </div>
                                                            <!-- /.form-group -->
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputReview">Review <span class="astk">*</span></label>
                                                                <textarea class="form-control txt txt-review" id="exampleInputReview" rows="4" placeholder=""></textarea>
                                                            </div>
                                                            <!-- /.form-group -->
                                                        </div>
                                                    </div>
                                                    <!-- /.row -->
                                                    <div class="action text-right">
                                                        <button class="btn btn-primary btn-upper">SUBMIT REVIEW</button>
                                                    </div>
                                                    <!-- /.action -->
                                                </form>
                                                <!-- /.cnt-form -->
                                            </div>
                                            <!-- /.form-container -->
                                        </div>
                                        <!-- /.review-form -->
                                    </div>
                                    <!-- /.product-add-review -->										
                                </div>
                                <!-- /.product-tab -->
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.product-tabs -->
            <!-- ============================================== UPSELL PRODUCTS ============================================== -->
            <section class="section featured-product wow fadeInUp">
                <h3 class="section-title">Customers who bought this product also bought</h3>
                <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag sale"><span>sale</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag sale"><span>sale</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag hot"><span>hot</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag new"><span>new</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        $650.99				</span>
                                        <span class="price-before-discount">$ 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/blank.gif')}}" data-echo="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag hot"><span>hot</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        $650.99				</span>
                                        <span class="price-before-discount">$ 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/blank.gif')}}" data-echo="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag new"><span>new</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        $650.99				</span>
                                        <span class="price-before-discount">$ 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                </div>
                <!-- /.home-owl-carousel -->
            </section>
            <!-- /.section -->
            <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
            <!-- ==============================================AUTHOR INFORMATION
                ============================================== -->
            <section class="section featured-product wow fadeInUp padding-30">
                <div class="row no-gutters">
                    <div class="col-sm-2">
                        <img class="img-responsive rounded-circle" src="{{asset('/public/home/images/author_d.webp')}}" alt="M. Scott Peck&nbsp;">
                    </div>
                    <div class="col-sm-10">
                        <p class="author-name"><a href="#">M. Scott Peck</a></p>
                        <div class="author-description-wrapper">
                            <div class="author-description" id="js--author-description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, at! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore cum tenetur pariatur hic commodi maiores possimus perferendis temporibus iure laudantium nam, quasi earum obcaecati architecto voluptate, nobis! Veniam magni doloribus, nobis ipsam delectus officiis, consequuntur enim numquam harum excepturi, perspiciatis accusamus blanditiis, quam voluptates nostrum! Asperiores maiores, fugit unde velit.</p>
                            </div>
                        </div>
                        <div class="author-description--read-more">
                            <p class="js--author-description--read-more">Read More</p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- ===================================================aUTHOR INFORMATION : END
                ============================================== -->
            <!-- ============================================== UPSELL PRODUCTS ============================================== -->
            <section class="section featured-product wow fadeInUp">
                <h3 class="section-title">Recently Viewed</h3>
                <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag sale"><span>sale</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag sale"><span>sale</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag hot"><span>hot</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag new"><span>new</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/blank.gif')}}" data-echo="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag hot"><span>hot</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                    <div class="item item-carousel">
                        <div class="products">
                            <div class="product">
                                <div class="product-image">
                                    <div class="image">
                                        <a href="{{url('/details_page')}}"><img  src="{{asset('/public/home/images/blank.gif')}}" data-echo="{{asset('/public/home/images/products/book-01.jpg')}}" alt=""></a>
                                    </div>
                                    <!-- /.image -->			
                                    <div class="tag new"><span>new</span></div>
                                </div>
                                <!-- /.product-image -->
                                <div class="product-info text-left">
                                    <h3 class="name"><a href="{{url('/details_page')}}">Floral Print Buttoned</a></h3>
                                    <div class="rating rateit-small"></div>
                                    <div class="description"></div>
                                    <div class="product-price">	
                                        <span class="price">
                                        TK.650.99				</span>
                                        <span class="price-before-discount">TK. 800</span>
                                    </div>
                                    <!-- /.product-price -->
                                </div>
                                <!-- /.product-info -->
                                <div class="cart clearfix animate-effect">
                                    <div class="action">
                                        <ul class="list-unstyled">
                                            <li class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>													
                                                </button>
                                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                                            </li>
                                            <li class="lnk wishlist">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Wishlist">
                                                <i class="icon fa fa-heart"></i>
                                                </a>
                                            </li>
                                            <li class="lnk">
                                                <a class="add-to-cart" href="{{url('/details_page')}}" title="Compare">
                                                <i class="fa fa-signal"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.action -->
                                </div>
                                <!-- /.cart -->
                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.products -->
                    </div>
                    <!-- /.item -->
                </div>
                <!-- /.home-owl-carousel -->
            </section>
            <!-- /.section -->
            <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
        </div>
        <!-- /.col -->
        <div class="clearfix"></div>
    </div>
    <!-- /.row -->
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
        <div class="logo-slider-inner">
            <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                <div class="item m-t-15">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand1.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item m-t-10">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand2.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand3.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand4.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand5.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand6.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand2.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand4.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand1.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
                <div class="item">
                    <a href="#" class="image">
                    <img data-echo="{{asset('/public/home/images/brands/brand5.png')}}" src="{{asset('/public/home/images/blank.gif')}}" alt="">
                    </a>	
                </div>
                <!--/.item-->
            </div>
            <!-- /.owl-carousel #logo-slider -->
        </div>
        <!-- /.logo-slider-inner -->
    </div>
    <!-- /.logo-slider -->
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	
</div>
<!-- /.container -->

<!-- Modal -->
<div class="modal fade" id="details_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4></h4>
          </div>
          <div class="modal-body">
            <div class="details_modal_img">
                <ul>
                    <li>
                        <img src="{{asset('/public/home/images/books/book-01.jpg')}}" alt="">
                    </li>
                    <li>
                        <img src="{{asset('/public/home/images/books/book-02.jpg')}}" alt="">
                    </li>
                    <li>
                        <img src="{{asset('/public/home/images/books/book-03.jpg')}}" alt="">
                    </li>
                </ul>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
    </div>
</div>
</div>
<!-- /.body-content -->

@endsection

@section('js')

@endsection