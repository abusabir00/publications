<!DOCTYPE html>
<html lang="en">

@include('front.includes.head')

<body>
<!-- main container of all the page elements -->
<div id="wrapper">
    <!-- Page Loader -->
    <div id="pre-loader" class="loader-container">
        <div class="loader">
            <img src="images/svg/rings.svg" alt="loader">
        </div>
    </div>
    <!-- W1 start here -->
    <div class="w1">
        @include('front.includes.header')



        <main id="mt-main">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 paddingbootom-md">    
                    @yield('slider')

                    @yield('mainContent')





                    </div>
                </div>
            </div>
        </main>

        @include('front.includes.footer')
    </div><!-- W1 end here -->
    <span id="back-top" class="fa fa-arrow-up"></span>
</div>
<!-- include jQuery -->
<script src="{{asset('/public/front/js/jquery.js')}}"></script>
<!-- include jQuery -->
<script src="{{asset('/public/front/js/plugins.js')}}"></script>
<!-- include jQuery -->
<script src="{{asset('/public/front/js/jquery.main.js')}}"></script>
</body>
</html>