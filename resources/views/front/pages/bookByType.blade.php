@extends('front.landingMaster')

@section('title')
Books Page
@endsection

@section('css')
<style type="text/css">
.box{
	padding: 7px 36px;
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
background-color: white;
}
</style>
@endsection

@section('mainContent')
<section class="mt-contact-banner style4 wow fadeInUp" data-wow-delay="0.4s" style="background-image: url(&quot;http://placehold.it/1920x205&quot;); visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;" style="padding-bottom:100px">
</section>
<!-- BEGIN PAGE BASE CONTENT -->

<ul class="mt-productlisthold list-inline">
@foreach($products as $product)	
	<li>
		<!-- mt product1 large start here -->
		<div class="mt-product1 large">
			<div class="box">
				<div class="b1">
					<div class="b2">
						<a href="{{url('/book/details/'.$product->id)}}"><img src="{{url('/public/uploads/books/'.$product->photo)}}" alt="image description"></a>
						<ul class="links">
							<li><a href="#"><i class="icon-handbag"></i><span>TK. {{$product->price}}</span></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="txt">
				<strong class="title"><a href="{{url('/book/details/'.$product->id)}}">Name: {{$product->name}}</a></strong>
				<strong class="title"><a href="product-detail.html">Name: {{$product->author}}</a></strong>
				<span class="price"></i> <span>Price: TK. {{$product->price}}</span></span>
			</div>
		</div><!-- mt product1 center end here -->
	</li>
@endforeach	
</ul>


{{ $products->links() }}
<!-- END PAGE BASE CONTENT -->
@endsection

@section('js')

@endsection