@extends('front.landingMaster')

@section('title')
Home Page
@endsection

@section('css')
<style type="text/css">
.box{
	padding: 7px 36px;
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
background-color: white;
}
</style>
@endsection

@section('slider')
@include('front.includes.slider')
@endsection

@section('mainContent')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info col-md-3">
	<div class="mt-productsc style2 wow fadeInUp" data-wow-delay="0.4s">
		<div class="row">

		</div>
		<!-- mt productscrollbar start here -->
		<div id="mt-productscrollbar black" class="row">
			<!-- mt holder start here -->
			<aside id="sidebar" class="col-md-12 wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
					<!-- shop-widget filter-widget of the Page start here -->
					<section class="shop-widget filter-widget bg-grey">
												<h2>CATEGORIES</h2>
						<!-- category list start here -->
						<ul class="list-unstyled category-list">
						@foreach($categories as $cat)
							<li>
								<a href="{{url('/book/list/cat/'.$cat->id)}}">
									<span class="name">{{$cat->name}}</span>
								</a>
									@foreach($cat['children'] as $subcat)
							        <br><a href="{{url('/book/list/cat/'.$subcat->id)}}">
									<span>---{{$subcat->name}}</span>
								</a>
							      @endforeach
							</li>
						@endforeach	
						</ul><!-- category list end here -->
					</section><!-- shop-widget filter-widget of the Page end here -->
				</aside>
		</div><!-- mt productscrollbar end here -->
	</div>
</div>

<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info col-md-9">
	<!-- mt productsc start here -->
	<div class="mt-productsc style2 wow fadeInUp" data-wow-delay="0.4s">
		<div class="row">
			<div class="col-xs-12 mt-heading text-uppercase text-center">
				<h2 class="heading">LATEST BOOK</h2>
				<p>FURNITURE DESIGNS IDEAS</p>
			</div>
		</div>
		<!-- mt productscrollbar start here -->
		<div id="mt-productscrollbar" class="row">
			<!-- mt holder start here -->
			<div class="mt-holder">
				<!-- mt product start here -->
				@foreach($bestProduct as $bPro)
				<div class="mt-product1 large">
					<!-- box start here -->
					<div class="box">
						<a href="{{url('/book/details/'.$bPro->id)}}"><img alt="image description" src="{{url('/public/uploads/books/'.$bPro->photo)}}" width="175"></a>
						<ul class="links">
							<li><a href="#">TK. {{$bPro->price}}</a></li>
							<li><a href="#"><span class="price-off">Tk {{$bPro->regPrice}}</span></a></li>
						</ul>
					</div><!-- box end here -->
					<!-- txt end here -->
					<div class="txt">
						<a href="{{url('/book/details/'.$bPro->id)}}"><strong class="title">Name: {{$bPro->name}} </strong></a>
						<span class="price"><span>TK. {{$bPro->price}}</span></span>
					</div><!-- txt end here -->
				</div>
				@endforeach
				<!-- mt product1 end here -->

				<!-- mt product start here -->
			</div><!-- mt holder end here -->
		</div><!-- mt productscrollbar end here -->
	</div>

	<!-- mt productsc start here -->
	<div class="mt-productsc style2 wow fadeInUp" data-wow-delay="0.4s">
		<div id="mt-productscrollbar" class="row">
			<div class="col-xs-12 mt-heading text-uppercase text-center">
				<h2 class="heading">BEST SELL BOOK</h2>
				<p>FURNITURE DESIGNS IDEAS</p>
			</div>
		</div>
		<!-- mt productscrollbar start here -->
		<div id="mt-productscrollbar" class="row">
			<!-- mt holder start here -->
			<div class="mt-holder">
				<!-- mt product start here -->
						<!-- mt productscrollbar start here -->
		<div id="mt-productscrollbar" class="row">
			<!-- mt holder start here -->
			<div class="mt-holder">
				<!-- mt product start here -->
				@foreach($latestProduct as $lPro)
				<div class="mt-product1 large">
					<!-- box start here -->
					<div class="box">
						<a href="{{url('/book/details/'.$lPro->id)}}"><img alt="image description" src="{{url('/public/uploads/books/'.$lPro->photo)}}" width="175"></a>
						<ul class="links">
							<li><a href="#">TK. {{$lPro->price}}</a></li>
							<li><a href="#"><span class="price-off">Tk {{$lPro->regPrice}}</span></a></li>
						</ul>
					</div><!-- box end here -->
					<!-- txt end here -->
					<div class="txt">
						<a href="{{url('/book/details/'.$lPro->id)}}"><strong class="title">Name: {{$lPro->name}} </strong></a>
						<span class="price"><span>TK. {{$lPro->price}}</span></span>
					</div><!-- txt end here -->
				</div>
				@endforeach
				<!-- mt product1 end here -->
				<!-- mt product start here -->
			</div><!-- mt holder end here -->
		</div><!-- mt productscrollbar end here -->


			</div><!-- mt holder end here -->
		</div><!-- mt productscrollbar end here -->
	</div>
	<!-- mt productsc end here -->

	<!-- mt productsc start here -->
	<div class="mt-productsc style2 wow fadeInUp" data-wow-delay="0.4s">
		<div class="row">
			<div class="col-xs-12 mt-heading text-uppercase text-center">
				<h2 class="heading"> NEW UPLOADED BOOKS </h2>
				<p>FURNITURE DESIGNS IDEAS</p>
			</div>
		</div>
		<!-- mt productscrollbar start here -->
		<div id="mt-productscrollbar" class="row">
			<!-- mt holder start here -->
			<div class="mt-holder">
				<!-- mt product start here -->
				@foreach($dealProduct as $dPro)
				<div class="mt-product1 large">
					<!-- box start here -->
					<div class="box">
						<a href="{{url('/book/details/'.$dPro->id)}}"><img alt="image description" src="{{url('/public/uploads/books/'.$dPro->photo)}}" width="175"></a>
						<ul class="links">
							<li><a href="#">TK. {{$dPro->price}}</a></li>
							<li><a href="#"><span class="price-off">Tk {{$dPro->regPrice}}</span></a></li>
						</ul>
					</div><!-- box end here -->
					<!-- txt end here -->
					<div class="txt">
						<a href="{{url('/book/details/'.$dPro->id)}}"><strong class="title">Name: {{$dPro->name}} </strong></a>
						<span class="price"><span>TK. {{$dPro->price}}</span></span>
					</div><!-- txt end here -->
				</div>
				@endforeach

			</div><!-- mt holder end here -->
		</div><!-- mt productscrollbar end here -->
	</div>
	<!-- mt productsc end here -->


</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('js')

@endsection