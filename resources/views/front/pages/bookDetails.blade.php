@extends('front.landingMaster')

@section('title')
Books Page
@endsection

@section('css')
<style type="text/css">

</style>
@endsection

@section('mainContent')

	<main id="mt-main">
		<!-- Mt Product Detial of the Page -->
		<section class="mt-product-detial wow fadeInUp" data-wow-delay="0.4s">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<!-- Slider of the Page -->
						<div class="slider">
							<!-- Product Slider of the Page -->
							<div class="product-slider">
								<div class="slide">
									<img src="{{url('/public/uploads/books/'.$products->photo)}}" alt="image descrption">
								</div>
							</div>
							<!-- Product Slider of the Page end -->

						</div>
						<!-- Slider of the Page end -->
						<!-- Detail Holder of the Page -->
						<div class="detial-holder">
							<h2>{{$products->name}}</h2>
							<!-- Rank Rating of the Page -->
							<div class="text-holder">
								<span class="price">TK.{{$products->price}} <del>{{$products->regPrice}}</del></span>
							</div>
							<!-- Product Form of the Page -->
							<div class="txt-wrap">
								<p><?php echo $products->note; ?></p>
							</div>
							<!-- Product Form of the Page end -->
							<ul class="list-unstyled list">
								<li><a href="#"><i class="fa fa-share-alt"></i>SHARE</a></li>
								<li><a href="#"><i class="fa fa-exchange"></i>COMPARE</a></li>
								<li><a href="#"><i class="fa fa-heart"></i>ADD TO WISHLIST</a></li>
							</ul>
						</div>
						<!-- Detail Holder of the Page end -->
					</div>
				</div>
			</div>
		</section><!-- Mt Product Detial of the Page end -->
	</main><!-- mt main end here -->


<!-- END PAGE BASE CONTENT -->
@endsection

@section('js')

@endsection




