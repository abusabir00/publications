<!-- footer of the Page -->
   <footer id="mt-footer" class="style5 wow fadeInUp" data-wow-delay="0.4s">
            <!-- Footer Holder of the Page -->
            <div class="footer-holder black">
                <div class="container-fluid">
                    <div class="row">
                        <nav class="col-xs-12 col-sm-8 col-md-9">
                            <!-- Footer Nav of the Page -->
                            <div class="nav-widget-1">
                                <h3 class="f-widget-heading">Categories</h3>
                                <ul class="list-unstyled f-widget-nav">
                                    <li><a href="#">Watches</a></li>
                                    <li><a href="#">Glasses</a></li>
                                    <li><a href="#">Bags</a></li>
                                    <li><a href="#">Shoes</a></li>
                                    <li><a href="#">Accessories</a></li>
                                </ul>
                            </div><!-- Footer Nav of the Page end -->
                            <!-- Footer Nav of the Page -->
                            <div class="nav-widget-1">
                                <h3 class="f-widget-heading">Information</h3>
                                <ul class="list-unstyled f-widget-nav">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#"> Terms &amp; Conditions</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Customer Service</a></li>
                                    <li><a href="#">FAQs</a></li>
                                </ul>
                            </div><!-- Footer Nav of the Page end -->
                            <!-- Footer Nav of the Page -->
                            <div class="nav-widget-1">
                                <h3 class="f-widget-heading">Account</h3>
                                <ul class="list-unstyled f-widget-nav">
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">Order Tracking</a></li>
                                    <li><a href="#">Wish List</a></li>
                                    <li><a href="#">Shopping Cart</a></li>
                                    <li><a href="#">Checkout</a></li>
                                </ul>
                            </div><!-- Footer Nav of the Page end -->
                        </nav>
                        <div class="col-xs-12 col-sm-4 col-md-3 text-right">
                            <!-- F Widget Newsletter of the Page -->
                            <div class="f-widget-newsletter">
                                <h3 class="f-widget-heading">Sing Up Newsletter</h3>
                                <div class="holder">
                                    <form action="#" class="newsletter-form">
                                        <fieldset>
                                            <input type="email" placeholder="Your Email" class="form-control">
                                            <button type="submit"><i class="fa fa-angle-right"></i></button>
                                        </fieldset>
                                    </form>
                                </div><!-- F Widget Newsletter of the Page end -->
                                <h4 class="f-widget-heading follow">Follow Us</h4>
                                <!-- Social Network of the Page -->
                                <ul class="list-unstyled social-network social-icon">
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
                                </ul><!-- Social Network of the Page end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Footer Holder of the Page end -->
            <!-- Footer Area of the Page -->
            <div class="footer-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <p>© <a href="index.html">schön.</a> - All rights Reserved</p>
                            <div class="bank-card align-center">
                                <img src="images/bank-card.png" alt="bank-card">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Footer area of the Page end -->
        </footer><!-- footer of the Page end -->