<section class="slider-3">
    <div class="banner-slider">
    @foreach($slider as $sl)    
        <div class="holder">
            <div class="img">
                <img src="{{url($sl->photo)}}" alt="image description">
            </div>
        </div>
    @endforeach    

    </div>
</section>