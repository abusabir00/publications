        <!-- mt header style7 start here -->
        <header id="mt-header" class="style7">
            <!-- mt-top-bar start here -->
            <div class="mt-top-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 hidden-xs">
                            <span class="tel active"> <i class="fa fa-phone" aria-hidden="true"></i> +01711242799</span>
                            <a class="tel" href="#"> <i class="fa fa-envelope-o" aria-hidden="true"></i>fulkuri@gmail.com</a>
                        </div>
                        <div class="col-xs-12 col-sm-6 text-right">
                            <!-- mt-top-list start here -->
                            <ul class="mt-top-list">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li class="active"><a href="#">Profile</a></li>
                            </ul><!-- mt-top-list end here -->
                        </div>
                    </div>
                </div>
            </div><!-- mt-top-bar end here -->
            <!-- mt-bottom-bar start here -->
            <div class="mt-bottom-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="mt-logo"><a href="{{url('/')}}"><img src="{{asset('/public/front/images/logo/mainlogo.png')}}" alt="schon"></a></div>
                            <!-- mt-icon-list start here -->
                            <ul class="mt-icon-list">
                                <li><a href="#" class="icon-user"></a></li>
                                <li class="drop">
                                    <a href="#" class="icon-heart cart-opener"></a>
                                    <!-- mt drop start here -->
                                </li>
                                <li class="drop">
                                    <a href="#" class="cart-opener">
                                        <span class="icon-handbag"></span>
                                    </a>
                                    <!-- mt drop start here -->
                                    <span class="mt-mdropover"></span>
                                </li>
                                <li class="hidden-lg hidden-md">
                                    <a class="bar-opener mobile-toggle" href="#">
                                        <span class="bar"></span>
                                        <span class="bar small"></span>
                                        <span class="bar"></span>
                                    </a>
                                </li>
                            </ul><!-- mt-icon-list end here -->
                            <!-- mt-search-box start here -->
                            <form action="{{url('/book/search/')}}" class="mt-search-box" method="POST">
                                <fieldset>
                                    <input type="text" name="seach" placeholder="Search">
                                    <button class="fa fa-search" type="submit"></button>
                                </fieldset>
                            </form><!-- mt-search-box end here -->
                            <!-- navigation start here -->
                            <nav id="nav">
                                <ul>
                                    <li>
                                        <a class="drop-link" href="{{url('/')}}">HOME <i class="fa fa-angle-down hidden-lg hidden-md" aria-hidden="true"></i></a>
                                        
                                    </li>
                                    <li class="drop">
                                        <a href="product-grid-view.html">BOOKS <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        <!-- mt dropmenu start here -->
                                        <div class="mt-dropmenu text-left">
                                        <!-- mt frame start here -->
                                        <div class="mt-frame">
                                            <!-- mt f box start here -->
                                            <div class="mt-f-box">
                                                <!-- mt col3 start here -->
                                                <div class="mt-col-3">
                                                @foreach($categories as $cat)    
                                                    <div class="sub-dropcont">
                                                        <strong class="title"><a href="{{url('/book/list/cat/'.$cat->id)}}" class="mt-subopener">{{$cat->name}}</a></strong>
                                                        <div class="sub-drop">
                                                        <ul>
                                                        @foreach($cat['children'] as $subcat)    
                                                        <li><a href="{{url('/book/list/cat/'.$subcat->id)}}">{{$subcat->name}}</a></li>
                                                        @endforeach
                                                        </ul>
                                                        </div>
                                                    </div>
                                                @endforeach    
                                                </div>
                                                <!-- mt col3 end here -->

                                                <!-- mt col3 start here -->
                                                <div class="mt-col-3">
                                                    <div class="sub-dropcont">
                                                        <strong class="title"><a href="#" class="mt-subopener">Classes</a></strong>
                                                        <div class="sub-drop">
                                                            <ul>
                                                            @foreach($clases as $class)    
                                                                <li><a href="{{url('/book/list/class/'.$class->id)}}">{{$class->name}}</a></li>
                                                            @endforeach    
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- mt col3 end here -->

                                                <!-- mt col3 start here -->
                                                <div class="mt-col-3">
                                                    <div class="sub-dropcont">
                                                        <strong class="title"><a href="#" class="mt-subopener">SUBJECTS</a></strong>
                                                        <div class="sub-drop">
                                                            <ul>
                                                            @foreach($subject as $sub)    
                                                                <li><a href="{{url('/book/list/subject/'.$class->id)}}"> {{$sub->name}}</a></li>
                                                            @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- mt col3 end here -->

                                                <!-- mt col3 start here -->
                                                <div class="mt-col-3 promo">
                                                    <div class="mt-promobox">
                                                        <a href="#"><img src="http://placehold.it/295x320" alt="promo banner" class="img-responsive"></a>
                                                    </div>
                                                </div>
                                                <!-- mt col3 end here -->
                                            </div>
                                            <!-- mt f box end here -->
                                        </div>
                                        <!-- mt frame end here -->
                                        </div>
                                        <!-- mt dropmenu end here -->
                                        <span class="mt-mdropover"></span>
                                    </li>
                                </ul>
                            </nav><!-- navigation end here -->
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- mt main start here -->