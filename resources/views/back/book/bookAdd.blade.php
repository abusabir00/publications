@extends('back.backMaster')

@section('title')
Admin|Dashboard
@endsection

@section('css')

@endsection

@section('mainPage')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="col-md-12">
@include('back.includes.message') 

    <div class="portlet box yellow">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>New Book</div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{url('/book/data/store')}}" method="POST" enctype="multipart/form-data">
            	{{csrf_field()}}
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label">Book Name</label>
                        <input type="text" class="form-control" placeholder="Enter Book Name" name="name">
                        <span class="help-block"> A block of help text. </span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Book Categories</label>
                        <div class="input-group">
                            <select class="form-control" name="bookCat">
                                <option value="">--Select--</option>
                                @foreach($categories as $cat)
                                <option value="{{$cat->id}}">{{$cat->name}}</option>
	                                @foreach($cat['children'] as $subcat)
							        <option value="{{$subcat->id}}">--{{$subcat->name}}</option>
							      @endforeach
                                @endforeach
                            </select> 
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Class Name</label>
                        <div class="input-group">
                           <select class="form-control" name="class">
                                <option value="">--Select--</option>
                                @foreach($clases as $class)
                                <option value="{{$class->id}}">{{$class->name}}</option>
                                @endforeach
                            </select> 
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Subject Name</label>
                        <div class="input-group">
                           <select class="form-control" name="subject">
                                <option value="">--Select--</option>
                                @foreach($subject as $sub)
                                <option value="{{$sub->id}}">{{$sub->name}}</option>
                                @endforeach
                            </select> 
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Author Name</label>
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input type="text" class="form-control" placeholder="Writer Name" name="author"> </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Selling Price</label>
                        <div class="input-icon">
                            <i class="fa fa-bell-o"></i>
                            <input type="number" class="form-control" placeholder="Selling Price" name="sPrice"> </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Regular Price</label>
                        <div class="input-icon right">
                            <i class="fa fa-microphone"></i>
                            <input type="number" class="form-control" placeholder="Regular Price" name="rPrice"> </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Book Quantity</label>
                        <div class="input-icon right">
                            <i class="fa fa-microphone"></i>
                            <input type="number" class="form-control" placeholder="Book Quantity" name="qty"> </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Book Picture</label>
                        <div class="input-icon right">
                            <i class="fa fa-microphone"></i>
                            <input type="file" class="form-control" name="photo"> </div>
                    </div>

                    <div class="form-group">
                            <div class="mt-checkbox-list">
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" name="latest" value="true"> Latest Book
                                    <span></span>
                                </label>
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" name="best" value="true"> Best Sell
                                    <span></span>
                                </label>
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox" name="deal" value="true"> Best Deal
                                    <span></span>
                                </label>
                                
                            </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Book Description</label>
                        <div class="input-icon right">
                            <textarea class="form-control" name="bookDesc"></textarea>
                        </div>    
                    </div>




                   

                <div class="form-actions">
                    <div class="btn-set pull-left">
                        <button type="submit" class="btn green">Submit</button>
                    </div>
                    <div class="btn-set pull-right">
                        <button type="button" class="btn default">Reset 1</button>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('js')

@endsection