@extends('back.backMaster')

@section('title')
Admin| Book Categories
@endsection

@section('css')
<link href="{{asset('/public/back/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/public/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('mainPage')
<!-- BEGIN PAGE BASE CONTENT -->

<div class="row">
	<div class="caption col-md-12" style="margin-bottom: 10px;"><a class="btn btn-small pull-right" href="{{url('/book/store')}}"><i class="fa fa-plus"></i> New Book</a></div>
    <div class="col-md-12">
    	

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-globe"></i>MANAGE BOOK </div>

                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        <th class="desktop"> # Serial No. </th>
                        <th class="desktop">Image</th>
                        <th class="desktop">Name</th>
                        <th class="all">Author</th>
                        <th class="min-tablet">Price</th>
                        <th class="min-tablet">Regular Price</th>
                        <th class="min-tablet"> Action </th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php $sl = 1; ?>   
                    @foreach($books as $c)
                        <tr>
                            <td>{{$sl}}</td>
                            <td><img src="{{url('/public/uploads/books/'.$c->photo)}}" width="60" height="60"></td>
                            <td>{{$c->name}}</td>
                            <td>{{$c->author}}</td>
                            <td>{{$c->price}}</td>
                            <td>{{$c->regPrice}}</td>
                            <td>
                            <button class="btn btn-info"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-danger"><i class="fa fa-close"></i></button>
                            </td>
                        </tr>
                    <?php $sl++; ?> 
                    @endforeach 
	


   
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>





<!-- END PAGE BASE CONTENT -->
@endsection

@section('js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('/public/back/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('/public/back/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/public/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="{{asset('/public/back/pages/scripts/table-datatables-responsive.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
$("#submit").click(function(){
    var name=$('#name').val();
    var status = $('form input[type=radio]:checked').val();
    var validate ='';

    if(name.trim() ==''){
        validate = validate+'Name is empty<br>';
    }
    if(status==null){
        validate = validate+'Status not selected ! <br>';
    }
    if(validate ==''){$("#setCat").click();}
    else{
        $('.alert-success').hide();
        $('.alert-danger').show();
        $('.alert-danger').html(validate);
    }
});

</script>
@endsection


