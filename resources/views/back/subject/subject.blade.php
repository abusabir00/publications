@extends('back.backMaster')

@section('title')
Admin| Book Categories
@endsection

@section('css')
<link href="{{asset('/public/back/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/public/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('mainPage')
<!-- BEGIN PAGE BASE CONTENT -->
@include('back.includes.message')

<div class="row">
	<div class="caption col-md-12" style="margin-bottom: 10px;"><button class="btn btn-small pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> New Subject</button></div>
    <div class="col-md-12">
    	

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-globe"></i>MANAGE SUBJECT </div>

                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        <th class="desktop"> # Serial No. </th>
                        <th class="desktop">Subject Name</th>
                        <th class="min-tablet">Status</th>
                        <th class="min-tablet">Publish Date</th>
                        <th class="min-tablet"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $sl = 1; ?>	
                    @foreach($subject as $c)
                    	<tr>
                    		<td>{{$sl}}</td>
                    		<td>{{$c->name}}</td>
                    		<td><h4> <span class="label label-primary">{{$c->status}}</span></h4></td>
                    		<td>{{$c->created_at}}</td>
                    		<td>
                    		<button class="btn btn-info"><i class="fa fa-pencil"></i></button>
                    		<button class="btn btn-danger"><i class="fa fa-close"></i></button>
                    		</td>
                    	</tr>
                    <?php $sl++; ?>	
                    @endforeach		


   
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>


<!-- Categories Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Add New Subject</h4>
    </div>
    <div class="modal-body">

    <!-- Onpage Error Message -->
	<div class="alert alert-success login-success" id="login-success" style="display:none"></div>
    <div class="alert alert-danger login-failed" id="login-danger" style="display:none"></div>	

      <form action="{{url('/subject/store')}}" method="POST">
      {{csrf_field()}}
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Subject Name</label>
		    <input type="text" class="form-control" id="name" placeholder="Enter Name..." name="name">
		  </div>
		  <div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="Active">
			  <label class="form-check-label" for="inlineRadio1">Active</label>
			</div>
			<div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="Inactive">
			  <label class="form-check-label" for="inlineRadio2">Inactive</label>
			</div>

    </div>
    <div class="modal-footer">
    <input type="submit" name="" id="setCat" hidden>
    <input type="button" id="submit" class="btn btn-success pull-left" value="Submit">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

    </form>

  </div>
  
</div>
</div>


<!-- END PAGE BASE CONTENT -->
@endsection

@section('js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('/public/back/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('/public/back/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/public/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="{{asset('/public/back/pages/scripts/table-datatables-responsive.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
$("#submit").click(function(){
    var name=$('#name').val();
    var status = $('form input[type=radio]:checked').val();
    var validate ='';

    if(name.trim() ==''){
        validate = validate+'Name is empty<br>';
    }
    if(status==null){
        validate = validate+'Status not selected ! <br>';
    }
    if(validate ==''){$("#setCat").click();}
    else{
        $('.alert-success').hide();
        $('.alert-danger').show();
        $('.alert-danger').html(validate);
    }
});

</script>
@endsection


