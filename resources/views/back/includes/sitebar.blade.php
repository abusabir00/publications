<div class="page-sidebar-wrapper">
<!-- BEGIN SIDEBAR -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item start ">
            <a href="{{url('/admin')}}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>

        <li class="heading">
            <h3 class="uppercase">Setup Options</h3>
        </li>

        <li class="nav-item  ">
            <a href="{{url('/book/categories/add')}}" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Book Categories</span>
            </a>
        </li>

        <li class="nav-item  ">
            <a href="{{url('/class/manage')}}" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Class</span>
            </a>
        </li>

        <li class="nav-item  ">
            <a href="{{url('/subject/manage')}}" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Subject</span>
            </a>
        </li>

        

        <li class="nav-item  ">
            <a href="{{url('/slider/add')}}" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Slider</span>
            </a>
        </li>

        <li class="heading">
            <h3 class="uppercase">Layouts</h3>
        </li>

        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-diamond"></i>
                <span class="title">Books</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{url('/book/store')}}" class="nav-link ">
                        <span class="title">Create Books</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{url('/book/data/list')}}" class="nav-link ">
                        <span class="title">Books List</span>
                    </a>
                </li>
            </ul>
        </li>

    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
</div>