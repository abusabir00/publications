@extends('back.backMaster')

@section('title')
Admin| Slider
@endsection

@section('css')
<link href="{{asset('/public/back/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/public/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('mainPage')
<!-- BEGIN PAGE BASE CONTENT -->

<div class="row">
	<div class="caption col-md-12" style="margin-bottom: 10px;"><button class="btn btn-small pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> New Slider</button></div>
  

    <div class="col-md-12">

    @if(Session::get('message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ Session::get('message') }}</strong>
    </div>
    @endif

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-globe"></i>MANAGE SLIDER </div>

                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        <th class="desktop"> # Serial No. </th>
                        <th class="desktop">Sider</th>
                        <th class="all">Name</th>
                        <th class="min-tablet">Publish Date</th>
                        <th class="min-tablet"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $num=1;  ?>
                    @foreach($slider as $sl)    
                        <tr>
                            <td>{{$num}}</td>
                            <td><img src="{{url($sl->photo)}}" width="180" height="56"></td>
                            <td>{{$sl->name}}</td>
                            <td>{{$sl->created_at}}</td>
                    
                    		<td>
                    		<a class="btn btn-danger" href="{{url('/slider/delete/'.$sl->id)}}" onclick=" confirm('Are you sure !!');"><i class="fa fa-close"></i></a> 
                    		</td>
                    	</tr><?php $num++; ?>
                    @endforeach    

	


   
                    </tbody>
                </table>
                    <span class="text-danger">{{ $errors->has('photo') ? $errors->first('photo') : '' }}</span>  
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>


<!-- Categories Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Add New Slider</h4>
    </div>
    <div class="modal-body">
    <!-- Onpage Error Message -->
    <div class="alert alert-success login-success" id="login-success" style="display:none"></div>
    <div class="alert alert-danger login-failed" id="login-danger" style="display:none"></div>      

      <form action="{{url('/slider/store')}}" method="POST" enctype="multipart/form-data">
      {{csrf_field()}}
		  <div class="form-group">
		    <label for="exampleFormControlInput1">Slider Name</label>
		    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Enter Name..." name="name">
            
		  </div>
		  <div class="form-group">
            <label for="exampleFormControlInput1">Slider Picture</label>
            <input type="file" class="form-control" id="photo" placeholder="Enter Name..." name="photo">
            
          </div>

		  <div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="Active">
			  <label class="form-check-label" for="inlineRadio1">Active</label>
			</div>
			<div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="Inactive">
			  <label class="form-check-label" for="inlineRadio2">Inactive</label>
			</div>

    </div>
    <div class="modal-footer">
    <input type="submit" name="" id="setSlider" hidden>
    <input type="button" id="submit" class="btn btn-success pull-left" value="Submit">
     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>

    </form>

  </div>
  
</div>
</div>


<!-- END PAGE BASE CONTENT -->
@endsection

@section('js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('/public/back/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('/public/back/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/public/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="{{asset('/public/back/pages/scripts/table-datatables-responsive.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
$("#submit").click(function(){
    var name=$('#photo').val();
    var status = $('form input[type=radio]:checked').val();
    var validate ='';
    var imgWidth = $("#photo").width();
    var imgHeight = $("#photo").height();

     // if(imgWidth != 1200){
     //     validate = validate+'Image width will be 1200 px! <br>';
     // }
     // if(imgHeight != 450){
     //     validate = validate+'Image height will be 450 px! <br>';
     // }

    if(name.trim() ==''){
        validate = validate+'Slider picture required ! <br>';
    }
    if(status==null){
        validate = validate+'Status not selected ! <br>';
    }
    if(validate ==''){$("#setSlider").click();}
    else{
        $('.alert-success').hide();
        $('.alert-danger').show();
        $('.alert-danger').html(validate);
    }
});

</script>
@endsection


