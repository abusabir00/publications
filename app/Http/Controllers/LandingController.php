<?php

namespace App\Http\Controllers;

use App\Slider;
use App\Subject;
use App\Cat;
use App\ClassList;
use Illuminate\Http\Request;
use DB;
use App\Book;

class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       return view('home.pages.home');
    }

    /**
     * Book Details
    */ 
    public function detailsPage()
    {
        return view('home.pages.details');
    }

    /**
     * Book list 
    */ 
    public function productsList()
    {
        return view('home.pages.products');
    }

    /**
     * welcome list 
    */ 
    public function welcome()
    {
        return view('welcome');
    }

    

    

    /**
     * Book list by Cat
    */
    public function bookCatList($id){
        $data['categories'] = Cat::tree();
        $data['clases'] = ClassList::where('status','Active')->get();
        $data['subject'] = Subject::where('status','Active')->get();
        $data['products'] = Book::where('status','Active')
                                ->where('cat',$id)
                                ->paginate(18);
        return view('front.pages.bookByType',$data);
    }

    /**
     * Book list by Class
    */
    public function bookClassList($id){
        $data['categories'] = Cat::tree();
        $data['clases'] = ClassList::where('status','Active')->get();
        $data['subject'] = Subject::where('status','Active')->get();
        $data['products'] = Book::where('status','Active')
                                ->where('class',$id)
                                ->paginate(18);
        return view('front.pages.bookByType',$data);
    }

    /**
     * Book list by Subject
    */
    public function bookSubList($id){
        $data['categories'] = Cat::tree();
        $data['clases'] = ClassList::where('status','Active')->get();
        $data['subject'] = Subject::where('status','Active')->get();
        $data['products'] = Book::where('status','Active')
                                ->where('class',$id)
                                ->paginate(18);
        return view('front.pages.bookByType',$data);
    }

    /**
     * Book Details by Subject
    */
    public function bookDetails($id){
        $data['categories'] = Cat::tree();
        $data['clases'] = ClassList::where('status','Active')->get();
        $data['subject'] = Subject::where('status','Active')->get();
        $data['products'] = Book::where('status','Active')
                                ->where('id',$id)
                                ->first();
        return view('front.pages.bookDetails',$data);

    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
