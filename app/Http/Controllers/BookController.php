<?php

namespace App\Http\Controllers;

use App\Book;
use App\Subject;
use App\Cat;
use App\ClassList;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use Session;
use File;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page'] = 'bookList';
        $data['books']=  Book::where('status','Active')->get();
    
        return view('back.book.bookList',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data['categories'] = Cat::tree();
        $data['clases'] = ClassList::where('status','Active')->get();
        $data['subject'] = Subject::where('status','Active')->get(); 
        return view('back.book.bookAdd',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $re)
    {
        //return $request->all();
        $input = Input::all();
        $rules = [
            'name' => 'required',
            'bookCat' => 'required',
            'class' => 'required',
            'subject' => 'required',
            'author' => 'required',
            'sPrice' => 'required',
            'rPrice' => 'required',
            'qty' => 'required',
            'photo' => 'required',
        ];
        $messages = [
            'name.required' => 'The book name field is required.',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        // Image Processing profile & Cover from Here---------    
        $book_image = $re->file('photo');
        $destinationPath = 'public/uploads/books/';    
        $extension = $book_image->getClientOriginalExtension();
        $file_name = rand(11111, 99999) . '.' . $extension;
        $book_image->move($destinationPath, $file_name);

        $book = new Book;
        $book->cat = $re->bookCat;
        $book->class = $re->class;
        $book->best = $re->best;
        $book->latest = $re->latest;
        $book->deal = $re->deal;
        $book->name = $re->name;
        $book->author = $re->author;
        $book->price = $re->sPrice;
        $book->regPrice = $re->rPrice;
        $book->quantity = $re->qty;
        $book->photo = $file_name;
        $book->note = $re->bookDesc;
        if($book->save()){
            Session::flash('success','Book Successfully aded !');
            return redirect()->back();
        }else{
            Session::flash('error','Book insert Error !!');
            return redirect()->back();
        }

    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}
