<?php

namespace App\Http\Controllers;

use App\ClassList;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use Session;


class ClassListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data['class'] = ClassList::where('status','Active')->get();
        return view('back.class.classList',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $input = Input::all();
        $rules = [
            'name' => 'required'];
        $messages = [
            'name.required' => 'The Class name field is required.',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $cat = new ClassList;
        $cat->name = $request->name;
        $cat->status = $request->status;
        if($cat->save()){
            Session::flash('success','Class Successfully aded !');
            return redirect()->back();
        }else{
            Session::flash('error','Class insert Error !!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClassList  $classList
     * @return \Illuminate\Http\Response
     */
    public function show(ClassList $classList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClassList  $classList
     * @return \Illuminate\Http\Response
     */
    public function edit(ClassList $classList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClassList  $classList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClassList $classList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClassList  $classList
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClassList $classList)
    {
        //
    }
}
