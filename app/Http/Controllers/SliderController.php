<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use DB;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['slider'] = Slider::where('status','Active')->get();
        return view('back.slider.sliderIndex',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  $this->validate($request, [
        //     'photo' => 'mimes:jpeg,jpg,png | dimensions:width=1200,height=730',
        // ]);
        $created_at = date('Y-m-d H:i:s');
        $name = $request->name;
        $sliderImage = $request->file('photo');
        $imageName = $sliderImage->getClientOriginalName();
        $uploadPath = 'public/back/upload/slider/';
        $sliderImage->move($uploadPath, $imageName);
        $imageUrl = $uploadPath . $imageName;
        
        $result =  DB::table('sliders')->insert([
            'name' => $request->name,
            'photo' => $imageUrl,
            'status'=> $request->status,
            'created_at' => $created_at
        ]);
         if($result){ return redirect('/slider/add')->with('message', 'New Slider Added successfully');}else{
           return redirect('/slider/add')->with('message', 'Error !!'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $photo = DB::table('sliders')
       ->select('photo')->where('id',$id)->first();
        $result = DB::table('sliders')->where('id',$id)->delete(); 
        if($result){ $del = unlink($photo->photo); }
        if($del){ return redirect('/slider/add')->with('message','Item Delete Successfully !'); }else{
        return redirect('/slider/add')->with('message','Somthing Error, Please Try Again !');} 
    }
}
