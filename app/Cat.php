<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    
    public function parent()
	{
	    return $this->belongsTo('App\Cat', 'id', 'parent'); // I believe you can use also hasOne().
	}

	public function children()
	{
	    return $this->hasMany('App\Cat', 'parent', 'id');
	}
	public static function tree() {

	    return static::where('parent', '=', 0)->get(); // or based on you question 0?

	}
}
