-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2018 at 07:34 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easy`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `seat` int(10) UNSIGNED NOT NULL,
  `cost` double(5,2) NOT NULL,
  `time` datetime NOT NULL,
  `from_point` int(10) UNSIGNED NOT NULL,
  `to_point` int(10) UNSIGNED NOT NULL,
  `trip_code` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Booked','Onboard','Done','Cancelled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Booked',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `user_id`, `vehicle_id`, `seat`, `cost`, `time`, `from_point`, `to_point`, `trip_code`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 1, 11.00, '2018-02-02 00:00:00', 0, 0, NULL, 'Cancelled', '2018-07-25 23:53:16', '2018-08-01 05:57:00'),
(3, 1, 1, 1, 11.00, '2018-07-28 06:04:11', 1, 4, NULL, 'Booked', '2018-07-28 00:08:54', '2018-07-28 00:08:54'),
(4, 1, 1, 1, 11.00, '2018-07-28 06:04:11', 1, 4, NULL, 'Booked', '2018-07-28 00:08:59', '2018-07-28 00:08:59'),
(5, 1, 1, 1, 11.00, '2018-07-28 06:04:11', 1, 4, NULL, 'Booked', '2018-07-28 00:09:06', '2018-07-28 00:09:06'),
(6, 1, 1, 1, 11.00, '2018-07-28 06:04:11', 1, 4, NULL, 'Booked', '2018-07-28 00:09:15', '2018-07-28 00:09:15'),
(7, 1, 2, 1, 11.00, '2018-07-28 06:04:11', 1, 4, NULL, 'Booked', '2018-07-28 00:10:50', '2018-07-28 00:10:50'),
(8, 1, 3, 1, 11.00, '2018-07-28 06:04:11', 1, 4, NULL, 'Booked', '2018-07-28 00:11:27', '2018-07-28 00:11:27'),
(9, 1, 1, 1, 11.00, '2018-07-28 06:04:11', 1, 4, 'W343RRT', 'Booked', '2018-07-31 23:37:34', '2018-07-31 23:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `coordinators`
--

CREATE TABLE `coordinators` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addr` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coordinators`
--

INSERT INTO `coordinators` (`id`, `name`, `email`, `phone`, `addr`, `password`, `token`, `created_at`, `updated_at`) VALUES
(1, 'Name Hossain', 'New@gmail.com', '01838198486', 'Dhaka', '827ccb0eea8a706c4c34a16891f84e7b', 'OWlmOTE1MG90MzR2Zm85Z29uYnlkcHM4M291bzBndm4=', '2018-07-06 10:12:11', '2018-07-10 13:48:58'),
(2, 'New Name', 'Name@gmail.com', '01920797864', 'Dhaka', '827ccb0eea8a706c4c34a16891f84e7b', 'cmp6NTJqZG50eWt5YjY2NXc3MThyenN2bDdudmUwbms=', '2018-07-03 07:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firebase_uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `password`, `firebase_uid`, `token`, `created_at`, `updated_at`) VALUES
(1, 'Name Hossain', 'New@gmail.com', '01920797864', '827ccb0eea8a706c4c34a16891f84e7b', 'wer44456777', 'ZTAybmRxdnlqNm5oYThnYnk2Z25pcDZ3OTVqeHNsMHA=', '2018-07-07 15:59:44', '2018-07-09 17:54:47'),
(3, 'fida', 'fida@shuttlebd.com', '+8801701395491', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Fl30jSAIXiPrRVf2aksNZ1H2p0z1', 'aWoyOWV0aXV6dmdkaWp1d2YxYW9yamwzMmV3a2lhODY=', '2018-07-11 16:44:49', '2018-07-19 01:46:18');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firebase_uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `name`, `email`, `phone`, `addr`, `password`, `firebase_uid`, `token`, `created_at`, `updated_at`) VALUES
(1, 'New Driver', 'driver@gmail.com', '01920797864', 'Dhaka', '202cb962ac59075b964b07152d234b70', '#tri', '123', '2018-07-06 10:12:11', '2018-07-07 15:59:44');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Gulshan 01', 'active', '2018-07-10 07:00:00', NULL),
(2, 'Gulshan 02', 'active', '2018-07-10 07:00:00', NULL),
(3, 'Banani', 'active', '2018-07-12 07:00:00', NULL),
(4, 'Niketon', 'active', '2018-07-12 07:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_07_051833_create_drivers_table', 1),
(4, '2018_07_07_060256_create_coordinators_table', 2),
(5, '2018_07_07_083910_create_customers_table', 3),
(6, '2018_07_11_051407_create_locations_table', 4),
(7, '2018_07_11_064234_create_points_table', 5),
(8, '2018_07_13_070007_create_vehicles_table', 6),
(9, '2018_07_18_181710_create_vehicle__assigns_table', 7),
(11, '2018_07_18_184603_create_trip__statuses_table', 8),
(13, '2018_07_18_223207_create_routes_table', 9),
(14, '2018_07_24_054740_create_route_costs_table', 10),
(15, '2018_07_26_053256_create_bookings_table', 11),
(16, '2018_07_30_041606_create_payments_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `trip_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `coordinator_id` int(10) UNSIGNED NOT NULL,
  `amount` double(8,2) UNSIGNED NOT NULL,
  `status` enum('Paid','Unpaid') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Unpaid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `trip_id`, `user_id`, `vehicle_id`, `coordinator_id`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 11.00, 'Unpaid', '2018-07-29 23:23:04', '2018-07-29 23:23:04');

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

CREATE TABLE `points` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` decimal(20,20) DEFAULT NULL,
  `lon` decimal(20,20) DEFAULT NULL,
  `location_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `name`, `lat`, `lon`, `location_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cercle', '0.99999999999999999999', NULL, '1', 'active', '2018-07-05 07:00:00', NULL),
(2, 'Cercle 02', NULL, NULL, '1', 'active', '2018-07-11 07:00:00', NULL),
(3, 'police plaza', '0.99999999999999999999', '0.99999999999999999999', '4', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `id` int(10) UNSIGNED NOT NULL,
  `from` int(10) UNSIGNED NOT NULL,
  `to` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`id`, `from`, `to`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2018-07-06 10:12:11', NULL),
(2, 2, 4, '2018-07-10 07:00:00', NULL),
(3, 3, 4, '2018-07-07 15:59:44', NULL),
(4, 1, 3, '2018-07-10 07:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `route_costs`
--

CREATE TABLE `route_costs` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_id` int(10) UNSIGNED NOT NULL,
  `to_id` int(10) UNSIGNED NOT NULL,
  `cost` double(5,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `route_costs`
--

INSERT INTO `route_costs` (`id`, `from_id`, `to_id`, `cost`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 50.00, '2018-07-05 21:12:11', NULL),
(2, 2, 4, 30.00, '2018-07-09 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trip__statuses`
--

CREATE TABLE `trip__statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `coordinator_id` int(10) UNSIGNED NOT NULL,
  `status` enum('start','pause','resume','stop') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'start',
  `available` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `route_id` int(10) UNSIGNED NOT NULL,
  `time` datetime NOT NULL,
  `trip_code` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trip__statuses`
--

INSERT INTO `trip__statuses` (`id`, `vehicle_id`, `coordinator_id`, `status`, `available`, `route_id`, `time`, `trip_code`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'start', 'no', 1, '2018-07-25 19:52:08', '', '2018-07-24 02:52:08', '2018-07-19 02:52:08'),
(2, 1, 2, 'start', 'yes', 1, '2018-07-25 00:00:00', '', '2018-07-24 02:59:44', NULL),
(3, 2, 2, 'start', 'yes', 1, '2018-07-25 00:00:00', '', '2018-07-23 18:00:00', NULL),
(4, 1, 1, 'start', 'yes', 1, '2018-08-01 04:53:26', '56HHty', '2018-07-31 22:53:26', '2018-07-31 22:53:26'),
(5, 1, 1, 'pause', 'no', 1, '2018-08-01 04:54:06', '{\"trip_code\":\"56HHty\"}', '2018-07-31 22:54:06', '2018-07-31 22:54:06'),
(6, 1, 1, 'start', 'yes', 1, '2018-08-01 05:01:55', 'NmFpd3l2', '2018-07-31 23:01:55', '2018-07-31 23:01:55'),
(7, 1, 1, 'pause', 'no', 1, '2018-08-01 05:02:07', '{\"trip_code\":\"NmFpd3l2\"}', '2018-07-31 23:02:07', '2018-07-31 23:02:07'),
(8, 1, 1, 'pause', 'no', 1, '2018-08-01 05:02:52', '{\"trip_code\":\"NmFpd3l2\"}', '2018-07-31 23:02:52', '2018-07-31 23:02:52'),
(9, 1, 1, 'pause', 'no', 1, '2018-08-01 05:04:01', '{\"trip_code\":\"NmFpd3l2\"}', '2018-07-31 23:04:01', '2018-07-31 23:04:01'),
(10, 1, 1, 'start', 'yes', 1, '2018-08-01 05:05:24', 'YW41bTh1', '2018-07-31 23:05:24', '2018-07-31 23:05:24'),
(11, 1, 1, 'pause', 'no', 1, '2018-08-01 05:05:37', 'YW41bTh1', '2018-07-31 23:05:37', '2018-07-31 23:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firebase_uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `name`, `capacity`, `registration`, `status`, `created_at`, `updated_at`) VALUES
(1, 'vehicles 01', '12', 'Dhaka)1', 'active', '2018-07-13 07:00:00', NULL),
(2, 'second', '12', 'Dhaka Ha new', 'active', '2018-07-24 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle__assigns`
--

CREATE TABLE `vehicle__assigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `coordinator_id` int(10) UNSIGNED NOT NULL,
  `driver_id` int(10) UNSIGNED NOT NULL,
  `assign_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle__assigns`
--

INSERT INTO `vehicle__assigns` (`id`, `vehicle_id`, `coordinator_id`, `driver_id`, `assign_date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-07-25', '2018-07-03 07:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coordinators`
--
ALTER TABLE `coordinators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route_costs`
--
ALTER TABLE `route_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trip__statuses`
--
ALTER TABLE `trip__statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle__assigns`
--
ALTER TABLE `vehicle__assigns`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `coordinators`
--
ALTER TABLE `coordinators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `points`
--
ALTER TABLE `points`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `route_costs`
--
ALTER TABLE `route_costs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `trip__statuses`
--
ALTER TABLE `trip__statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle__assigns`
--
ALTER TABLE `vehicle__assigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
