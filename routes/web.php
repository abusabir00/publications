<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front End Controller from Here =========
Route::get('/welcome','LandingController@welcome');

Route::get('/','LandingController@index');
Route::get('/details_page','LandingController@detailsPage');
Route::get('/products_list','LandingController@productsList');

Route::get('/book/list/cat/{id}','LandingController@bookCatList');
Route::get('/book/list/class/{id}','LandingController@bookClassList');
Route::get('/book/list/subject/{id}','LandingController@bookSubList');
Route::get('/book/details/{id}','LandingController@bookDetails');
Route::post('/book/search/','LandingController@search');



// Back End Controller from Here ===========

Auth::routes();
Route::get('/admin', 'HomeController@index');
Route::get('/dashboard','adminController@index');
Route::get('/book/categories/add','CatController@index');
Route::post('/book/categories/store','CatController@store');
Route::get('/book/store','BookController@add');
Route::get('/book/data/list','BookController@index');
//Class anage Rout
Route::get('/class/manage','ClassListController@index');
Route::post('/class/store','ClassListController@store');

//Class anage Rout
Route::get('/subject/manage','SubjectController@index');
Route::post('/subject/store','SubjectController@store');


Route::post('/book/data/store','BookController@store');

Route::get('/slider/add','SliderController@index');
Route::post('/slider/store','SliderController@store');
Route::get('/slider/delete/{id}','SliderController@destroy');