<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('cat')->unsigned();
            $table->tinyInteger('class')->unsigned();
            $table->tinyInteger('sellType')->unsigned();
            $table->string('name',192);
            $table->string('author',192);
            $table->decimal('price', 8, 2);
            $table->decimal('regPrice', 8, 2);
            $table->integer('quantity',10);
            $table->string('photo',192)->nullable();
            $table->text('note')->nullable();
            $table->enum('status', ['Active', 'Inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
